import elastic.peliculas.Pelicula
import elastic.peliculas.PeliculasSystem
import io.javalin.http.Context
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class Respuesta(val respuesta: String?)
data class PeliculaDTO(val original_title: String?,
                       val original_language: String?,
                       val id: Long?,
                       val overview: String?,
                       val poster_path: String?,
                       val release_date: String?)

//
//
//data class UserDTO(val name: String, var image: String, var id:String)
//
//
//data class RespuestaUserDTO(val id: String, val name: String, var image: String, var followers: MutableList<UserDTO>)



//class FormatTime(){
//    fun darleFormato(date: LocalDateTime): String{
//        val transformar = DateTimeFormatter.ofPattern("dd/mm/yy - hh:mm")
//        return date.format(transformar)
//    }
//}

class PeliculasController(val system: PeliculasSystem) {


//    fun pelicula(ctx: Context) {
//        val token = ctx.header("Authorization")
//        try {
//            val userId = tokenController.validateToken(token!!)
//            val user = system.getUser(userId)
//            val posteos = (system.searchByUserId(userId).map{
//                PostSinCommentDTO(it.id,
//                    it.description,
//                    it.portrait,
//                    it.landscape,
//                    likesDTO(it.likes),
//                    FormatTime().darleFormato(it.date),
//                    (UserDTO(it.user.name,it.user.image,it.user.id)),
//                )
//            }).toMutableList()
//            val timeline = (system.timeline(userId).map {
//                PostSinCommentDTO(it.id,
//                    it.description,
//                    it.portrait,
//                    it.landscape,
//                    likesDTO(it.likes),
//                    FormatTime().darleFormato(it.date),
//                    (UserDTO(it.user.name,it.user.image,it.user.id)),
//                )
//            })
//            ctx.json(RespuestaUserTimelineDTO(user.id ,user.name, user.image, followerDTO(user.followers), timeline ,posteos))
//        } catch (e: NotFound) {
//            ctx.status(401).json(e.message!!)
//        }
//    }

//    fun getPelicula(ctx: Context) {
//        val id = ctx.pathParam("id")
//        try {
//            val user = system.getUser(id)
//            val post = (system.searchByUserId(id).map {
//                PostSinCommentDTO(it.id,
//                    it.description,
//                    it.portrait,
//                    it.landscape,
//                    likesDTO(it.likes),
//                    FormatTime().darleFormato(it.date),
//                    UserDTO(it.user.name, it.user.image, it.user.id))
//            }).toMutableList()
//            val followers = (user.followers.map { UserDTO(it.name,it.image, it.id) }).toMutableList()
//            ctx.json(RespuestaUserDTO(user.id, user.name, user.image, followers, post))
//            ctx.status(200)
//        }catch (e:NotFound){
//            ctx.status(404).json(Respuesta("Usuario con el id ${id} no encontrado"))
//        }
//    }



    fun getContenido(ctx: Context){
        val text = ctx.queryParam("q")
        var peliculas:List<PeliculaDTO>
        peliculas = (system.busquedaPorLucene(text!!).map {
            PeliculaDTO(it.original_title, it.original_language,it.id,it.overview,it.poster_path,it.release_date)
        })
//            val posteosDTO = posts.map { PostSinCommentDTO(it.id, it.description, it.portrait,
//                it.landscape, likesDTO(it.likes), FormatTime().darleFormato(it.date), UserDTO(it.user.name, it.user.image, it.user.id)) }
        ctx.json(peliculas)

    }
}
