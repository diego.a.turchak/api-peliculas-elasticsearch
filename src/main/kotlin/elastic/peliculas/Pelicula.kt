package elastic.peliculas
class Pelicula{


    var original_title: String? = null
    var original_language: String? = null
    var id: Long? = null
    var overview: String? = null
    var poster_path: String? = null
    var release_date: String? = null

    protected constructor(){}

    constructor(original_title:String,original_language:String, id:Long,overview:String,poster_path:String, release_date:String){
        this.original_title = original_title
        this.original_language = original_language
        this.id = id
        this.overview = overview
        this.poster_path = poster_path
        this.release_date = release_date
    }
}