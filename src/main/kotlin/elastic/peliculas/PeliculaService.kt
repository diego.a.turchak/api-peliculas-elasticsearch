package elastic.peliculas

interface PeliculaService {

    fun busquedaPorLucene(palabra:String):List<Pelicula>
    fun busquedaEspecificaPorTitulo(titulo:String):List<Pelicula>
}