package elastic.peliculas
import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER
import org.apache.http.HttpHost
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import java.io.IOException


class PeliculaDAO {

    /** conexion con elastic **/
    val conection = RestHighLevelClient(RestClient.builder(HttpHost("commit-a-commit.com", 9200, "http")))

    /** BUSCADOR ESPECIFICO POR TITULO**/
    fun peliculasPorTitulo(nombre: String) : List<Pelicula>{
        val searchRequest = SearchRequest()
        searchRequest.indices("peliculas")
        val searchSourceBuilder = SearchSourceBuilder()
        //  searchSourceBuilder.query(QueryBuilders.matchQuery("original_title", nombre))
        searchSourceBuilder.query(QueryBuilders.matchPhraseQuery("original_title", nombre))
        return searchRequestSource(searchRequest, searchSourceBuilder)
    }

    fun busquedaLucene(nombre: String) : List<Pelicula>{
        val searchRequest = SearchRequest()
        searchRequest.indices("peliculas")
        val searchSourceBuilder = SearchSourceBuilder()
        //searchSourceBuilder.query(QueryBuilders.matchQuery(Search))
        searchSourceBuilder.query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("original_title", nombre)));
        return searchRequestSource(searchRequest, searchSourceBuilder)
    }

    fun searchRequestSource(searchRequest: SearchRequest, searchSourceBuilder: SearchSourceBuilder) : List<Pelicula>{
        searchRequest.source(searchSourceBuilder)
        var listaRetorno = mutableListOf<Pelicula>()
        var searchResponse : SearchResponse? = null
        try {
            searchResponse = conection.search(searchRequest, RequestOptions.DEFAULT)
            if(searchResponse.hits.totalHits.value > 0){
                val searchHit = searchResponse.hits.hits
                for (hit in searchHit){
                    LOGGER.info("Documento con id ${hit.id}: ${hit.sourceAsString}")
                    val map = hit.sourceAsMap
                    listaRetorno.add(ObjectMapper().convertValue(map, Pelicula::class.java))
                }
            }
        }catch (e: IOException){
            e.printStackTrace()
        }
        return listaRetorno
    }

    fun conexion(){
        val client = RestHighLevelClient(RestClient.builder(HttpHost("localhost", 9200, "http")))
        LOGGER.info("Cliente conectado.")

        val searchRequest = SearchRequest("peliculas")
        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.query(QueryBuilders.matchAllQuery())
        searchRequest.source(searchSourceBuilder)

        val searchResponse = client.search(searchRequest, RequestOptions.DEFAULT)

        for (hit in searchResponse.hits.hits) {
            LOGGER.info("Documento con id ${hit.id}: ${hit.sourceAsString}")
        }

        client.close()
        LOGGER.info("Cliente desconectado.")
    }


}

fun main() {
    val dao = PeliculaDAO()
    // dao.conexion()
    //var lista = dao.peliculasPorTitulo("Toy Story")
    var lista = dao.busquedaLucene("Toy Story")
}