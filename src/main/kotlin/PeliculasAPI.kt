import elastic.peliculas.PeliculasSystem
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.core.util.RouteOverviewPlugin


class PeliculasAPI {

    fun start() {

        val system = PeliculasSystem()
        val peliculaController = PeliculasController(system)
        val app = Javalin.create {
            it.defaultContentType = "aplication/json"
            it.registerPlugin(RouteOverviewPlugin("/routes"))
            it.accessManager(PeliculasAccessManager(system))
            it.enableCorsForAllOrigins()
        }

        app.before {
            it.header("Access-Control-Expose-Headers", "*")
        }

        app.routes {


//            path("pelicula") {
//                get(peliculaController::pelicula)
//                path(":id") {
//                    get(peliculaController::getPelicula)
//
//                }
//            }
            path("search"){
                get(peliculaController::getContenido, setOf(PeliculasRoles.ANYONE))
            }
        }
        app.start(8000)
    }

}

fun main(args: Array<String>) {

    PeliculasAPI().start()
}